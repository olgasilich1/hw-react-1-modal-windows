import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Button.scss';

export const Button = ({backgroundColor, text, onClick, dataName, className}) => {
  const classes = classNames(
    'button',
    className
  )

  // const classes = classNames(
  //   'button',
  //   className,
  // );

  return(
    <button className={classes} data-name={dataName} style={{backgroundColor: backgroundColor}} onClick={onClick}>{text}</button>
  )
}


Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
  classNames: PropTypes.string,
}

Button.defaultProps = {
  backgroundColor: '',
  text: '',
  onClick: () => {},
  classNames: '',
}