import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Modal.scss';

const Modal = ({
  header,
  body,
  actions,
  onCalcel,
  closeButton,
  backgroundColor,
}) => {
  return (
    <div className="modalOverLay" onClick={onCalcel}>
      <div className="modalWindow" style={{ backgroundColor: backgroundColor }}>
        <div className="modalHeader">
          {header}
          {closeButton && (
            <span onClick={onCalcel} style={{ cursor: 'pointer' }}>
              X
            </span>
          )}
        </div>
        <div className="modalBody" dangerouslySetInnerHTML={{ __html: body }} />
        <div className="modalFooter">{actions}</div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string,
  body: PropTypes.string,
  actions: PropTypes.node,
  closeButton: PropTypes.bool,
  onCalcel: PropTypes.func,
};

Modal.defaultProps = {
  header: '',
  body: '',
  actions: null,
  closeButton: false,
  onCalcel: () => {},
};

export default Modal;
