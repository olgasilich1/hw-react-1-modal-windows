import React, { useState } from 'react';
import { Button } from './components/button/Button';
import Modal from './components/modal/Modal';

import './App.css';

function App() {
  // const [isOpen, setOpen] = useState(null);
  const [openedModalName, setOpenedModalName] = useState(null);

  const openModal = (e) => {
    // setOpen(true);
    setOpenedModalName(e.target.getAttribute('data-name'));
  };

  const closeModal = (e) => {
    // e.stopPropagation();
    if (e.target !== e.currentTarget) {
      return;
    }
    setOpenedModalName(null);
  };

  const modals = {
    first: {
      name: 'first',
      headerText: 'Do you want to delete this file?',
      bodyText:
        "Once you delete this file, it won't be possible to undo this action. <br /> Are you shure you want to delete it?",
      actions: [
        <Button className="modalButton" text="Ok" />,
        <Button onClick={closeModal} className="modalButton" text="Cancel" />,
      ],
      backgroundColor: 'rgb(160, 5, 5)',
      closeButton: true,
    },
    second: {
      name: 'second',
      headerText: 'Have you ever been in Puerto-Rico?',
      bodyText:
        'We are proposing you an amausing tour to San-Juan. <br /> Are you ready fot traveling?',
      actions: [
        <Button className="modalButton" text="Yes" />,
        <Button onClick={closeModal} className="modalButton" text="No" />,
      ],
      backgroundColor: 'rgb(7, 87, 87)',
      closeButton: false,
    },
  };

  // const header = openedModalName === 'first' ? 'Do you want to delete this file?' : 'Have you ever been in Puerto-Rico?';
  // const body = openedModalName === 'first' ? "Once you delete this file, it won't be possible to undo this action. \n Are you shure you want to delete it?" : "We are proposing you an amausing tour to San-Juan. \n Are you ready fot traveling?";
  // const btnYesText = openedModalName === 'first' ? 'Ok' : 'Yes';
  // const btnNoText = openedModalName === 'first' ? 'Cancel' : 'No';
  // const backgroundColor = openedModalName === 'first' ? 'rgb(160, 5, 5)' : 'rgb(7, 87, 87)';

  const activeModal = modals[openedModalName];

  return (
    <div className="App">
      <div>
        <Button
          dataName="first"
          backgroundColor="rgb(160, 5, 5)"
          text="Open first modal"
          onClick={openModal}
        ></Button>
        <Button
          dataName="second"
          backgroundColor="rgb(7, 87, 87)"
          text="Open second modal"
          onClick={openModal}
        ></Button>
      </div>

      {activeModal && (
        <Modal
          backgroundColor={activeModal.backgroundColor}
          header={activeModal.headerText}
          body={activeModal.bodyText}
          onCalcel={closeModal}
          closeButton={activeModal.closeButton}
          actions={activeModal.actions}
        />
      )}
    </div>
  );
}

export default App;
